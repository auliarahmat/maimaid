import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import Feather from 'react-native-vector-icons/Feather';
import DropDownPicker from 'react-native-dropdown-picker';
import md5 from 'md5';
import axios from 'axios';
import baseUrl from '../../config';

const UpdateScreen = ({route, navigation}) => {
  console.log(route.params);

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    {label: 'Male', value: 1},
    {label: 'Famale', value: 2},
  ]);

  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirm, setConfirm] = useState('');
  const [dateOfBirth, setDateOfBirth] = useState('');

  const [validationEmail, setValidationEmail] = useState(false);
  const [validationConfirm, setValidationConfirm] = useState(false);
  const [validationGender, setValidationGender] = useState(false);
  const [validationDob, setValidationDob] = useState(false);
  const [validationName, setValidationName] = useState(false);
  const [validationPasswords, setValidationPasswords] = useState(false);
  const [secureText, setSecureText] = useState(true);
  const [dateValidate, setDateValidate] = useState(false);

  useEffect(() => {
    getView();
    isValidDate(dateOfBirth);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getView = () => {
    axios
      .post(baseUrl + 'view', {id: route.params})
      .then(e => {
        const dt = e.data.data;
        setFullName(dt.fullname);
        setEmail(dt.email);
        setPassword(dt.password);
        setDateOfBirth(dt.dob);
        setValue(dt.gender);

        console.log(e.data);
      })
      .catch(err => console.log(err));
  };

  const updateData = async () => {
    if (!validationName && fullName) {
      if (!validationEmail && email) {
        if (!validationGender && value) {
          if (!dateValidate && dateOfBirth) {
            const req = {
              id: route.params,
              fullname: fullName,
              email,
              password: md5(password),
              gender: value,
              dob: dateOfBirth,
            };

            console.log(!dateValidate);

            await axios
              .post(baseUrl + 'update', req)
              .then(e => {
                console.log('berhasil', e.data);
                if (e.data.status.code === 200) {
                  navigation.goBack();
                }
              })
              .catch(err => console.log(err.response));
          } else {
            setValidationDob(true);
          }
        } else {
          setValidationGender(true);
        }
      } else {
        setValidationEmail(true);
      }
    } else {
      setValidationName(true);
    }
  };

  const validatedConfirm = e => {
    setConfirm(e);
    if (e !== password) {
      setValidationConfirm(true);
    } else {
      setValidationConfirm(null);
    }
  };

  const validatedPassword = e => {
    setPassword(e);

    var pattern = new RegExp('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$');
    if (!pattern.test(e) || e === '') {
      setValidationPasswords(true);
    } else if (e.length < 6) {
      setValidationPasswords(true);
    } else {
      setValidationPasswords(false);
    }
  };

  const validatedName = e => {
    setFullName(e);
    if (e === '' || e.length < 3) {
      setValidationName(true);
    } else {
      setValidationName(false);
    }
  };

  const validatedEmail = e => {
    setEmail(e);
    if (
      e !== '' &&
      /^\w+([+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email)
    ) {
      setValidationEmail(false);
    } else {
      setValidationEmail(true);
    }
  };

  const isValidDate = dateString => {
    setDateOfBirth(dateString);
    let regEx = /^\d{4}-\d{2}-\d{2}$/;
    if (dateString.match(regEx) === null) {
      setDateValidate(true);
    } else {
      setDateValidate(false);
    }
  };

  return (
    <>
      <ScrollView style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.text}>Full Name</Text>
          <TextInput
            value={fullName}
            onChangeText={e => validatedName(e)}
            style={styles.textInput}
            // autoCapitalize='sentences'
          />
          {validationName && (
            <Text style={styles.textValidation}>Fullname is required</Text>
          )}
        </View>

        <View style={styles.content}>
          <Text style={styles.text}>Email</Text>
          <TextInput
            value={email}
            onChangeText={e => validatedEmail(e)}
            style={styles.textInput}
            keyboardType="email-address"
          />
          {validationEmail && (
            <Text style={styles.textValidation}>Email invalid</Text>
          )}
        </View>

        <View style={styles.content}>
          <Text style={styles.text}>Password</Text>
          <View style={styles.row}>
            <TextInput
              //   value={password}
              onChangeText={e => validatedPassword(e)}
              style={styles.textInput}
              secureTextEntry={secureText}
            />
            <TouchableOpacity
              style={styles.btnIcons}
              onPress={() => setSecureText(!secureText)}>
              <Feather
                name={secureText ? 'eye' : 'eye-off'}
                size={20}
                color="grey"
                style={styles.icons}
              />
            </TouchableOpacity>
          </View>
          {validationPasswords && (
            <Text style={styles.textValidation}>
              Password invalid (combination of letters and numbers)
            </Text>
          )}
        </View>

        <View style={styles.content}>
          <Text style={styles.text}>Confirm Password</Text>
          <TextInput
            //   value={password}
            onChangeText={e => validatedConfirm(e)}
            onFocus={validatedConfirm}
            style={styles.textInput}
            secureTextEntry={true}
          />
          {validationConfirm && (
            <Text style={styles.textValidation}>Confirm password invalid</Text>
          )}
        </View>

        <View style={styles.content}>
          <Text style={styles.text}>Gender</Text>
          <DropDownPicker
            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
            style={styles.dropDown}
          />
          {validationGender && (
            <Text style={styles.textValidation}>Gender is required</Text>
          )}
        </View>

        <View style={[styles.content, {marginBottom: 20}]}>
          <Text style={styles.text}>Date Of Birth</Text>
          <TextInput
            onChangeText={e => isValidDate(e)}
            style={styles.textInput}
            keyboardType="number-pad"
            value={dateOfBirth}
          />
          {dateValidate && (
            <Text style={styles.textValidation}>
              date format must be yyyy-mm-dd
            </Text>
          )}
        </View>
      </ScrollView>
      <View style={styles.containerBtn}>
        <TouchableOpacity onPress={updateData} style={styles.btn}>
          <Text style={styles.textBtn}>Update User</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default UpdateScreen;
