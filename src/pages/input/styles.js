import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    padding: 15,
    backgroundColor: 'white',
    flex: 1,
    borderTopWidth: 0.3,
    borderTopColor: 'rgb(220, 220, 220)',
  },
  content: {
    marginTop: 20,
  },
  text: {
    //
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    borderBottomWidth: 0.8,
    borderBottomColor: 'rgb(200, 200, 200)',
    height: 37,
    fontSize: 16,
    paddingBottom: -4,
    width: '100%',
    paddingRight: 23,
    marginBottom: 3,
  },
  textValidation: {
    color: 'red',
    fontSize: 11,
  },
  btnIcons: {
    height: 30,
    width: 30,
    position: 'absolute',
    right: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icons: {
    position: 'absolute',
    right: 6,
  },
  dropDown: {
    borderWidth: 0,
    borderBottomWidth: 0.8,
    borderBottomColor: 'rgb(200, 200, 200)',
  },

  containerBtn: {
    paddingHorizontal: 18,
    paddingVertical: 10,
    backgroundColor: 'white',
    width: '100%',
  },
  btn: {
    backgroundColor: 'dodgerblue',
    width: '100%',
    height: 40,
    borderRadius: 7,
    justifyContent: 'center',
  },
  textBtn: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
  },
});
