import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import styles from './styles';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import DropDownPicker from 'react-native-dropdown-picker';
import md5 from 'md5';
import axios from 'axios';
import baseUrl from '../../config';

const InputScreen = ({navigation}) => {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    {label: 'Male', value: 1},
    {label: 'Famale', value: 2},
  ]);
  const [checkBox, setCheckBox] = useState(false);

  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirm, setConfirm] = useState('');
  const [dateOfBirth, setDateOfBirth] = useState('');

  const [validationEmail, setValidationEmail] = useState(false);
  const [validationConfirm, setValidationConfirm] = useState(false);
  const [validationGender, setValidationGender] = useState(false);
  const [validationDob, setValidationDob] = useState(false);
  const [validationName, setValidationName] = useState(false);
  const [validationPasswords, setValidationPasswords] = useState(false);
  const [secureText, setSecureText] = useState(true);
  const [dateValidate, setDateValidate] = useState(false);

  const addUser = async () => {
    if (!validationName && fullName) {
      if (!validationEmail && email) {
        if (!validationPasswords && password) {
          if (!validationConfirm && confirm) {
            if (!validationGender && value) {
              if (!dateValidate && dateOfBirth) {
                const req = {
                  fullname: fullName,
                  email,
                  password: md5(password),
                  gender: value,
                  dob: dateOfBirth,
                };

                await axios
                  .post(baseUrl + 'create', req)
                  .then(e => {
                    navigation.goBack();
                  })
                  .catch(err => console.log(err));
              } else {
                setValidationDob(true);
              }
            } else {
              setValidationGender(true);
            }
          } else {
            setValidationConfirm(true);
          }
        } else {
          setValidationPasswords(true);
        }
      } else {
        setValidationEmail(true);
      }
    } else {
      setValidationName(true);
    }
  };

  const validatedConfirm = e => {
    setConfirm(e);
    if (e !== password) {
      setValidationConfirm(true);
    } else {
      setValidationConfirm(null);
    }
  };

  const validatedPassword = e => {
    setPassword(e);

    var pattern = new RegExp('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$');
    if (!pattern.test(e) || e === '') {
      setValidationPasswords(true);
    } else if (e.length < 6) {
      setValidationPasswords(true);
    } else {
      setValidationPasswords(false);
    }
  };

  const validatedName = e => {
    setFullName(e);
    if (e === '' || e.length < 3) {
      setValidationName(true);
    } else {
      setValidationName(false);
    }
  };

  const validatedEmail = e => {
    setEmail(e);
    if (
      e !== '' &&
      /^\w+([+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email)
    ) {
      setValidationEmail(false);
    } else {
      setValidationEmail(true);
    }
  };

  const isValidDate = dateString => {
    setDateOfBirth(dateString);
    let regEx = /^\d{4}-\d{2}-\d{2}$/;
    if (dateString.match(regEx) === null) {
      setDateValidate(true);
    } else {
      setDateValidate(false);
    }
  };

  const validateAgree = () => {
    if (
      !validationName &&
      fullName &&
      !validationEmail &&
      email &&
      !validationPasswords &&
      password &&
      !validationConfirm &&
      confirm &&
      !validationGender &&
      value &&
      !dateValidate &&
      dateOfBirth
    ) {
      setCheckBox(!checkBox);
    }

    console.log('dob', !dateValidate);
  };

  return (
    <>
      <ScrollView style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.text}>Full Name</Text>
          <TextInput
            onChangeText={e => validatedName(e)}
            style={styles.textInput}
            // autoCapitalize='sentences'
          />
          {validationName && (
            <Text style={styles.textValidation}>Fullname is required</Text>
          )}
        </View>

        <View style={styles.content}>
          <Text style={styles.text}>Email</Text>
          <TextInput
            onChangeText={e => validatedEmail(e)}
            style={styles.textInput}
            keyboardType="email-address"
          />
          {validationEmail && (
            <Text style={styles.textValidation}>Email invalid</Text>
          )}
        </View>

        <View style={styles.content}>
          <Text style={styles.text}>Password</Text>
          <View style={styles.row}>
            <TextInput
              onChangeText={e => validatedPassword(e)}
              style={styles.textInput}
              secureTextEntry={secureText}
            />
            <TouchableOpacity
              style={styles.btnIcons}
              onPress={() => setSecureText(!secureText)}>
              <Feather
                name={secureText ? 'eye' : 'eye-off'}
                size={20}
                color="grey"
                style={styles.icons}
              />
            </TouchableOpacity>
          </View>
          {validationPasswords && (
            <Text style={styles.textValidation}>
              Combination of letters and numbers (minimal 6 character)
            </Text>
          )}
        </View>

        <View style={styles.content}>
          <Text style={styles.text}>Confirm Password</Text>
          <TextInput
            onChangeText={e => validatedConfirm(e)}
            onFocus={validatedConfirm}
            style={styles.textInput}
            secureTextEntry={true}
          />
          {validationConfirm && (
            <Text style={styles.textValidation}>Confirm password invalid</Text>
          )}
        </View>

        <View style={styles.content}>
          <Text style={styles.text}>Gender</Text>
          <DropDownPicker
            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
            style={styles.dropDown}
          />
          {validationGender && (
            <Text style={styles.textValidation}>Gender is required</Text>
          )}
        </View>

        <View style={[styles.content]}>
          <Text style={styles.text}>Date Of Birth</Text>
          <TextInput
            onChangeText={e => isValidDate(e)}
            style={styles.textInput}
            keyboardType="number-pad"
          />
          {validationDob ? (
            <Text style={styles.textValidation}>Date of birth is required</Text>
          ) : (
            dateValidate && (
              <Text style={styles.textValidation}>
                date format must be yyyy-mm-dd
              </Text>
            )
          )}
        </View>

        <View
          style={[
            styles.content,
            {marginBottom: 20, flexDirection: 'row', alignItems: 'center'},
          ]}>
          {checkBox ? (
            <TouchableOpacity onPress={() => setCheckBox(false)}>
              <Ionicons name="checkbox" size={27} color="dodgerblue" />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity disabled={checkBox} onPress={validateAgree}>
              <Image
                style={{
                  height: 30,
                  width: 30,
                }}
                source={require('../../assets/icons/checkbox.png')}
              />
            </TouchableOpacity>
          )}
          <Text>I agree with the applicable terms</Text>
        </View>
      </ScrollView>
      <View style={styles.containerBtn}>
        <TouchableOpacity
          disabled={!checkBox}
          onPress={addUser}
          style={[styles.btn, !checkBox && {backgroundColor: 'lightskyblue'}]}>
          <Text style={styles.textBtn}>Add User</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default InputScreen;
