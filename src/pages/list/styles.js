import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    padding: 15,
    flex: 1,
    backgroundColor: 'white',
    borderTopWidth: 0.3,
    borderTopColor: 'rgb(220, 220, 220)',
  },
  content: {
    borderWidth: 1,
    borderColor: 'rgb(220, 220, 220)',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
  },
  btnUpdate: {
    width: '100%',
    height: 27,
    backgroundColor: 'deepskyblue',
    marginBottom: 15,
    marginTop: -2,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textUpdate: {
    color: 'white',
  },
  column: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  text: {
    marginLeft: 7,
  },

  containerAdd: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    backgroundColor: 'dodgerblue',
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
  },
});
