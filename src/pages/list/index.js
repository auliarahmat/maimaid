import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  ActivityIndicator,
  Image,
} from 'react-native';
import styles from './styles';
import Feather from 'react-native-vector-icons/Feather';

import baseUrl from '../../config';
import axios from 'axios';

const List = ({navigation}) => {
  const [data, setData] = useState(null);

  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getData();
    });

    return unsubscribe;
  }, [navigation]);

  const toAdd = () => {
    navigation.navigate('Input');
  };

  const getData = () => {
    const paginationReq = {
      page: 1,
      offset: 6,
    };

    axios
      .post(baseUrl + 'read', paginationReq)
      .then(e => {
        setData(e.data.data.rows);
        console.log('data', e.data.data.rows);
      })
      .catch(err => console.log(err));
  };

  const handleLoad = () => {
    setLoadMore(true);

    const paginationReq = {
      page: page,
      offset: 6,
    };

    axios
      .post(baseUrl + 'read', paginationReq)
      .then(e => {
        const {status, data: res} = e.data || {};
        if (status.code === 200 && res.total > 0) {
          setData([...data, ...res.rows]);
          setPage(page + 1);
          setLoadMore(false);
        } else {
          setLoadMore(false);
        }
      })
      .catch(err => console.log(err));
  };

  const toUpdate = id => {
    console.log('id', id);
    navigation.navigate('Update', id);
  };

  const renderFooter = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        {loadMore ? (
          <>
            {/* <ActivityIndicator size="large" /> */}
            <Text>Load more ...</Text>
          </>
        ) : null}
      </View>
    );
  };

  const BasicPage = item => {
    const dt = item.item.item;
    const genderData = dt.gender === 1 ? 'Male' : 'Famale';

    return (
      <>
        <View style={styles.content}>
          <View style={styles.column}>
            <View style={styles.row}>
              <Feather name="user" size={20} />
              <Text style={styles.text}>{dt.fullname}</Text>
            </View>

            <View style={styles.row}>
              <Feather name="mail" size={20} />
              <Text style={styles.text}>{dt.email}</Text>
            </View>

            {/* <View style={styles.row}>
            <Feather name="key" size={20} />
            <Text style={styles.text}>{dt.password ? dt.password :}</Text>
          </View> */}
          </View>

          <View>
            <View style={styles.row}>
              <Feather name="calendar" size={20} />
              <Text style={styles.text}>{dt.dob}</Text>
            </View>

            <View style={styles.row}>
              <Image
                style={{
                  height: 22,
                  width: 22,
                }}
                source={require('../../assets/icons/gender.png')}
              />
              <Text style={styles.text}>{genderData}</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => toUpdate(dt.id)}
          style={styles.btnUpdate}>
          <Text style={styles.textUpdate}>Update</Text>
        </TouchableOpacity>
      </>
    );
  };

  return (
    <>
      <StatusBar
        backgroundColor="dodgerblue"
        barStyle="dark-content"
        translucent
      />
      <SafeAreaView style={styles.container}>
        <FlatList
          data={data}
          renderItem={item => <BasicPage item={item} />}
          keyExtractor={(x, idx) => `${idx}`}
          showsVerticalScrollIndicator={false}
          ListFooterComponent={renderFooter}
          onEndReached={() => handleLoad()}
          onEndReachedThreshold={0.7}
        />

        <TouchableOpacity onPress={toAdd} style={styles.containerAdd}>
          <Feather name="plus" size={20} color="white" />
        </TouchableOpacity>
      </SafeAreaView>
    </>
  );
};

export default List;
